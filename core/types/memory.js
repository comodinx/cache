'use strict';

const minimatch = require('minimatch');
const memoryCache = require('memory-cache');
const Base = require('./abstract');
const _ = require('../helpers');

class Cache extends Base {
    constructor (options) {
        super();

        if (this.isEnabled) {
            this.client = new memoryCache.Cache(options);
        }
    }

    async get (key, defaultValue) {
        if (!this.isEnabled) {
            return defaultValue;
        }

        const value = await this.client.get(key);

        if (!_.isNil(value)) {
            return JSON.parse(value);
        }
        return defaultValue;
    }

    async put (key, value, time) {
        if (!this.isEnabled) {
            return value;
        }

        if (_.isObject(key)) {
            time = value;
        }

        if (!_.isObject(key)) {
            if (_.isNil(value)) {
                return this.remove(key);
            }
            else {
                key = {
                    [key]: value
                };
            }
        }

        _.each(key, async (v, k) => await this.client.put(k, JSON.stringify(v), time));
        return value;
    }

    async remove (key) {
        if (!this.isEnabled) {
            return;
        }

        if (!_.isArray(key)) {
            key = [key];
        }

        return _.each(key, (v, k) => this.client.del(k));
    }

    async removeMatch (pattern) {
        if (!this.isEnabled) {
            return;
        }

        const keys = await this.client.keys();

        return _.each(keys, async key => {
            if (minimatch(key, pattern)) {
                await this.client.del(key);
            }
        });
    }

    async keys (pattern) {
        if (!this.isEnabled) {
            return;
        }

        const keys = await this.client.keys();

        return (keys || []).reduce((acc, key) => {
            if (!pattern || minimatch(key, pattern)) {
                acc.push(key);
            }
        }, []);
    }

    async clear () {
        if (!this.isEnabled) {
            return;
        }
        return this.client.clear();
    }
}

module.exports = Cache;
