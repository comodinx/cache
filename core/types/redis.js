'use strict';

const { promisify } = require('util');
const redis = require('redis');
const Base = require('./abstract');
const _ = require('../helpers');

const host = process.env.CACHE_HOST || 'localhost';
const port = process.env.CACHE_PORT || 6379;
const password = process.env.CACHE_PASS || process.env.CACHE_PASSWORD || undefined;

class Cache extends Base {
    constructor (options) {
        super();

        if (this.isEnabled) {
            options = options || {};

            this.client = redis.createClient({
                socket: { host, port },
                legacyMode: true,
                password,
                ...options
            });
            this.client.getAsync = promisify(this.client.get).bind(this.client);
            this.client.setAsync = promisify(this.client.set).bind(this.client);
            this.client.delAsync = promisify(this.client.del).bind(this.client);
            this.client.keysAsync = promisify(this.client.keys).bind(this.client);
            this.client.quitAsync = promisify(this.client.quit).bind(this.client);
            this.client.pingAsync = promisify(this.client.ping).bind(this.client);

            if (!['0', 'false'].includes(String(options.autoConnect).toLowerCase())) {
                try {
                    (async () => await this.client.connect())();
                }
                catch (e) {
                    console.warn('Can\'t connect cache type redis', e);
                }
            }

            this.client.on('error', (error) => {
                if (!(error instanceof redis.SocketClosedUnexpectedlyError)) {
                    console.error(error);
                }
            });
        }
    }

    async get (key, defaultValue) {
        if (!this.isEnabled) {
            return defaultValue;
        }

        const value = await this.client.getAsync(key);

        if (!_.isNil(value)) {
            return JSON.parse(value);
        }
        return defaultValue;
    }

    async put (key, value, time) {
        if (!this.isEnabled) {
            return value;
        }

        if (_.isObject(key)) {
            time = value;
        }

        if (!_.isObject(key)) {
            if (_.isNil(value)) {
                return this.remove(key);
            }
            else {
                key = {
                    [key]: value
                };
            }
        }

        _.each(key, (v, k) => {
            if (time) {
                return this.client.setAsync(k, JSON.stringify(v), 'PX', time);
            }
            return this.client.setAsync(k, JSON.stringify(v));
        });
        return value;
    }

    async remove (keys) {
        if (!this.isEnabled) {
            return;
        }
        return this.client.delAsync(_.isArray(keys) ? keys : [keys]);
    }

    async removeMatch (pattern) {
        if (!this.isEnabled) {
            return;
        }

        const keys = await this.client.keysAsync(pattern);

        if (!keys || !keys.length) {
            return keys;
        }
        return this.remove(keys);
    }

    async keys (pattern) {
        if (!this.isEnabled) {
            return;
        }

        return this.client.keysAsync(pattern);
    }

    async clear () {
        return this.removeMatch('*');
    }

    /**
     * @deprecated This function only exists for backwards compatibility. Use quit function
     */
    async end (flush) {}

    async quit () {
        if (!this.isEnabled) {
            return;
        }

        return this.client.quitAsync();
    }

    configure (options) {
        if (this.isEnabled) {
            _.extend(this.client.options, options || {});
        }
    }
}

module.exports = Cache;
