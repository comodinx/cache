'use strict';

const enabled = !['0', 'false'].includes(String(process.env.CACHE_ENABLED).toLowerCase());

const types = {
    milliseconds: 'MILLISECOND',
    seconds: 'SECOND',
    minutes: 'MINUTE',
    hours: 'HOUR',
    days: 'DAY',
    months: 'MONTH',
    years: 'YEAR'
};

class AbstractCache {
    static get types () {
        return types;
    }

    get isEnabled () {
        return enabled;
    }

    prepareKey (key) {
        return key.toLowerCase();
    }
}

module.exports = AbstractCache;
