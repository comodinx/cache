'use strict';

const apicache = require('./apicache');
const Cache = require(`./types/${process.env.CACHE_TYPE || 'memory'}`);
const cache = new Cache();
const instances = {};

function _defineCache (name) {
    Object.defineProperty(cache, name, {
        configurable: false,
        enumerable: true,
        get: function () {
            let instance = instances[name];

            if (!instance) {
                instance = instances[name] = require(`./types/${name}`);
            }
            return instance;
        }
    });
}

_defineCache('memory');
_defineCache('redis');

cache.apicache = apicache;

module.exports = cache;
