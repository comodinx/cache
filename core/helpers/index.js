'use strict';

const isArray = Array.isArray;

function isNil (value) {
    // eslint-disable-next-line
    return value == null;
}

function isObject (value) {
    const type = typeof value;

    // eslint-disable-next-line
    return value != null && (type == 'object' || type == 'function');
}

function isObjectLike (value) {
    // eslint-disable-next-line
    return value != null && typeof value == 'object';
}

function isEqual (value, other) {
    if (Object.is(value, other)) {
        return true;
    }

    if (value === other) {
        return true;
    }
    // eslint-disable-next-line
    if (value == null || other == null || (!isObjectLike(value) && !isObjectLike(other))) {
        // eslint-disable-next-line
        return value !== value && other !== other;
    }

    const keys = Object.keys(value);
    return Object.keys(other).every(i => keys.indexOf(i) !== -1) && keys.every(i => isEqual(value[i], other[i]));
}

function each (list, iterator) {
    if (isArray(list)) {
        return list.forEach(iterator);
    }
    return Object.keys(list).forEach(key => iterator(list[key], key, list));
}

module.exports = {
    each,
    isNil,
    isArray,
    isEqual,
    isObject
};
